#!/usr/bin/bash
## Module 5 Homework
## Create a script that lets the user enter a temperature in either
## Fahrenheit, Celsius, or Kelvin then recieves the equilvalent temperature
## in the other two units as the output

read -p "Enter a temperature unit of your choice(F,C,K): " unit
read -p "Enter the number value for your selected temperature: " temp
if [ $unit = "F" ]
then
	F="$temp"
	C="$(echo "scale=2;($F - 32) / 1.8" | bc )"
	K="$(echo "scale=2;($C + 273.15)" | bc )"
	echo "Fahrenheit = " $F 
	echo "Celsius = " $C
	echo "Kelvin = " $K
elif [ $unit = "C" ]
then
        C="$temp"
	K="$(echo "scale=2;($C + 273.15)" | bc )"
        F="$(echo "scale=2;($C * 1.8) + 32" | bc )"
        echo "Fahrenheit = " $F 
        echo "Celsius = " $C
        echo "Kelvin = " $K
elif [ $unit = "K" ]
then
        K="$temp"
        C="$(echo "scale=2;($K - 273.15)" | bc )"
        F="$(echo "scale=2;($C * 1.8) + 32" | bc )"
        echo "Fahrenheit = " $F 
        echo "Celsius = " $C
        echo "Kelvin = " $K
else 
	echo "Temperature unit is not supported."

fi
